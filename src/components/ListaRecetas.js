import React, { useContext } from 'react';

// ************************************** CUSTOM COMPONENT ***************************** //

import Receta from './Receta';

// ************************************************************************************* //

// ************************************* CONTEXT API *********************************** //

import { RecetasContext } from '../context/RecetasContext';


// ************************************************************************************* //

const ListaRecetas = () => {

    // EXTRACT THE RECIPES
    const { recetas } = useContext( RecetasContext );

    console.log( recetas );

    return ( 
        <div className="row mt-5 " >
            { recetas.map( receta => (

                <Receta
                    key={receta.idDrink}
                    receta={ receta } 
                />

            )) }
        </div>
     );
}
 
export default ListaRecetas;