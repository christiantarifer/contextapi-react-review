import React, { createContext, useEffect, useState } from 'react';

// ************************************** CUSTOM COMPONENT ***************************** //

import axios from 'axios';

// ************************************************************************************* //

//  CREATE CONTEXT
export const ModalContext = createContext();

const ModalProvider = (props) => {

    // STATE DEL PROVIDER
    const [ idreceta, guardarIdReceta ] = useState(null);

    const [ informacion, guardarReceta ] = useState({



    })

    // ONCE WE HAVE THE RECIPE, MAKE THE API CALL
    useEffect( () => {

        const obtenerReceta = async () => {

            if ( !idreceta ) return;

            const url = `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${idreceta}`;

            const resultado = await axios.get(url);

            guardarReceta( resultado.data.drinks[0] );

        }

        obtenerReceta();

    }, [ idreceta ]);

    return (
        <ModalContext.Provider
            value={{
                informacion,
                guardarIdReceta,
                guardarReceta
            }}
        >
            {props.children}
        </ModalContext.Provider>
    );

}

export default ModalProvider;