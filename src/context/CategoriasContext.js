import React, { createContext, useState, useEffect } from 'react';

// ***************************** THIRD PARTY LIBRARIES *************************** //

import axios from 'axios';

// ********************************************************************* //

// CREATE THE CONTEXT
export const CategoriasContext = createContext();

/**
 * PROVIDER
 * Elements that provides functions and states
 */
const CategoriasProvider = (props) => {

    // CREATE CONTEXT STATE
    const [ categorias, guardarCategorias ] = useState([]);

    // MAKE API CALL
    useEffect( () => {

        const obtenerCategorias = async () => {

            const url = 'https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list';

            const categorias = await axios.get( url );

            guardarCategorias(categorias.data.drinks);

        }

        obtenerCategorias();

    }, [  ]);

    return(

        <CategoriasContext.Provider
            value={{
                categorias
            }}
        >
            { props.children }
        </CategoriasContext.Provider>

    )


}

export default CategoriasProvider;